# YgHealthcare - Healthcare Website

Tools to simply the process of building deep learning systems.

## Development Setup

- Clone Repository: `git clone HTTPS/SSH Url`
- Move to root directory: `cd yghealthcare`
- Running command: `http-server`

## Tools & Packages Used
- Visutal Studio code
- Bootstrap 4.5
- Jquery
- Slick Carousel

## Testing Server

[Start Testing](http://yghealthcare.s3-website-us-east-1.amazonaws.com/)

## Developer Best Practice

- Maintain proper namespacing for folders, files, variable and function declarations.
- Always create feature or bug branches and then merge with stable master branch.
- Provide proper commit messages & split commits meaningfully.
