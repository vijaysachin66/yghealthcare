$('.patient-slider').slick({
    infinite: true,
    arrows: true,
    dots: true,
    autoplay: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<div class="slick-left"><img src="assets/images/leftarrow.png"></div>',
    prevArrow: '<div class="slick-right"><img src="assets/images/rightarrow.png"></div>',
  });

  $('.news-slider').slick({
    infinite: true,
    arrows: true,
    dots: true,
    autoplay: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<div class="slick-left"><img src="assets/images/leftarrow.png"></div>',
    prevArrow: '<div class="slick-right"><img src="assets/images/rightarrow.png"></div>',
  });